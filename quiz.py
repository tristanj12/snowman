import sys
import os

score = 0
answered = []

def question_loop(answer, question):
    print("PLEASE NOTE: Type 'back' to return to question selection. Type 'hint' for a hint.\n")
    global score
    global answered
    while True:
        ans = input("enter your answer: ")
        if ans.lower() in answer.lower():
            print("Well done!")
            score += 10
            answered.append(question)
            break
        elif ans.lower() == "back":
            print("Returning to question selection")
            break
        elif ans.lower() == "hint":
            print(hints[question])
        else:
            if ans.lower() == "shorter" or ans.lower() == "longer":
                print("Very clever...")
            elif len(ans.split()) < len(answer.split()):
                print("Not quite. The answer you're looking for is longer")
            elif len(ans.split()) > len(answer.split()):
                print("Not quite. The answer you're looking for is shorter")

def one():
    print("Gather the morse code from the keygen programme")
    print("How should you respond?")
    answer = "Hell shall be their home: an evil fate"
    question_loop(answer, "1")

def two():
    print("Gather the base64 code from the keygen programme then submit translation here")
    answer = "Gods sight is Islam"
    question_loop(answer, "2")

def three():
    print("Figure out the cipher used, then submit translation for the following:")
    print("WNJ OMK DUK RNQFKGD LNPPJRZDW KEKM MOZGKS JT XNM PORIZRS. WNJ KRYNZR YJGDZLK ORS XNMQZS KEZF. WNJ QKFZKEK ZR HNS.")
    answer = "YOU ARE THE NOBLEST COMMUNITY EVER RAISED UP FOR MANKIND. YOU ENJOIN JUSTICE AND FORBID EVIL. YOU BELIEVE IN GOD."
    question_loop(answer, "3")

def four():
    print("Figure out the cipher used, then submit translation for the following:")
    print("ECD'E EVEVX QK YOFY JPW FHBPYOQB! CJIX KT GKTI LSQ NSYKZ QBAF CKAN ZORFGSRG TLGC SYPYZ KLOHZ. OY INLM GAF'T BZG GKZDCLJQGK, ANBYQNWE HHMV IR VADAPC IPLMSI BEZ WYZWRM TA YIBP AT ILNFDUA XOII HHYSP FWS EGSIDGIY!")
    answer = "GOD'S CURSE BE UPON THE INFIDELS! EVIL IS THAT FOR WHICH THEY HAVE BARTERED AWAY THEIR SOULS. TO DENY GOD'S OWN REVELATION, GRUDGING THAT HE SHOULD REVEAL HIS BOUNTY TO WHOM HE CHOOSES FROM AMONG HIS SERVANTS!"
    question_loop(answer, "4")

def five():
    print("Figure out the cipher used, then submit translation for the following:")
    print("fb c5 99 d5 d8 de db c3 8c 9d d8 96 d8 8c 8d dc c7 9b 9d c3 98 9d eb 91 d9 8c 8a d5 c3 8d d8 8c 8a d5 cd 8a 9d ca 97 da c4 8a 9d cd 99 dc c5 90 ce d8 de c4 c3 8b 91 8c 9c c8 d8 de d9 c3 de d3 c3 8a 9d cd 8a c9 cd 9d d6 8c 8a d5 c9 93 9d ca 97 cf df 8a 93 8c b9 d2 c8 de d9 c3 9b ce 8c 90 d2 d8 de d1 c3 88 d8 8c 8a d5 c9 de dc cb 99 cf c9 8d ce c3 8c ce 82")
    answer = "Fight for the sake of God those that fight against you, but do not attack them first. God does not love the aggressors."
    question_loop(answer, "5")

def exit():
    print("Thanks for playing!")
    print(f"Final score: {score}/50")

options = {"1" : one,
           "2" : two,
           "3" : three,
           "4" : four,
           "5" : five,
           "exit" : exit
}

hints =   {"1" : "I'm sending out an 'dot dot dot dash dash dash dot dot dot'\n",
           "2" : "Hmmm that padding == familiar\n",
           "3" : "Hmmm, what if I swapped this for something else?...\n",
           "4" : "If only I had the key to success. French style!",
           "5" : "You might have been hexed, but these are exclusively different!"
}

def main():
    global answered
    print("Welcome to the cyber challenge!\n   Please refer to the scenario sheet provided!\n   Type exit at the main menu to finish")
    while True:
        if len(answered) == 5:
            print("You've cracked the ciphers, but haven't found the app. Are there any interesting strings left in that binary that can help you?")
            options["exit"]()
            break
        try:
            txt = input("\nChoose a question from 1-5:" )
            if txt in answered:
                print("You've already answered that one!")
            else:
                options[txt]()
        except KeyError:
            print("invalid selection")
        except KeyboardInterrupt:
            options["exit"]()
            break

if __name__ == "__main__":
    main()
