#include <iostream>
#include <string>
#include <Windows.h>

int level_one() {
    //Prophet, make war on the unbelievers and the hypocrites and deal rigorously with them. 
    std::cout << ".--. .-. --- .--. .... . -/-- .- -.- ./.-- .- .-./--- -./- .... ./..- -. -... . .-.. .. . ...- . .-. .../.- -. -../- .... ./.... -.-- .--. --- -.-. .-. .. - . .../.- -. -../-.. . .- .-../.-. .. --. --- .-. --- ..- ... .-.. -.--/.-- .. - ..../- .... . --" << std::endl;
    return 0;
}

int level_two() {
    //gods sight is Islam
    std::cout << "Z29kcyBzaWdodCBpcyBJc2xhbQ==" << std::endl;
    return 0;
}

int level_three() {
    //WNJ OMK DUK RNQFKGD LNPPJRZDW KEKM MOZGKS JT XNM PORIZRS. WNJ KRYNZR YJGDZLK ORS XNMQZS KEZF. WNJ QKFZKEK ZR HNS.
    std::cout << "OQLSKXHUZYIFPRNTBMGDJEACWV" << std::endl;
    return 0;
}

int level_four() {
    //ECD'E EVEVX QK YOFY JPW FHBPYOQB! CJIX KT GKTI LSQ NSYKZ QBAF CKAN ZORFGSRG TLGC SYPYZ KLOHZ. OY INLM GAF'T BZG GKZDCLJQGK, ANBYQNWE HHMV IR VADAPC IPLMSI BEZ WYZWRM TA YIBP AT ILNFDUA XOII HHYSP FWS EGSIDGIY! 
    std::cout << "YOAMCBNDTPGEZRLQISXUWHVKFJ" << std::endl;
    return 0;
}

int level_five() {
    //fb c5 99 d5 d8 de db c3 8c 9d d8 96 d8 8c 8d dc c7 9b 9d c3 98 9d eb 91 d9 8c 8a d5 c3 8d d8 8c 8a d5 cd 8a 9d ca 97 da c4 8a 9d cd 99 dc c5 90 ce d8 de c4 c3 8b 91 8c 9c c8 d8 de d9 c3 de d3 c3 8a 9d cd 8a c9 cd 9d d6 8c 8a d5 c9 93 9d ca 97 cf df 8a 93 8c b9 d2 c8 de d9 c3 9b ce 8c 90 d2 d8 de d1 c3 88 d8 8c 8a d5 c9 de dc cb 99 cf c9 8d ce c3 8c ce 82
    std::cout << "IVMSBDOZWQLXATGJHRUCPKFYNE" << std::endl;
    return 0;
}

int level_hidden() {
    std::cout << "How many times cooler are you than the ACC? :";
    std::string input;
    std::getline(std::cin, input);
    if (std::stoi(input) != 5) {
        std::cout << "HACK DETECTED" << std::endl << "Deleting system32";
        while (true) {
            Sleep(500);
            std::cout << ".";
        }
    }
    else {
        std::cout << "https://giphy.com/gifs/retro-thumbs-up-XreQmk7ETCak0" << std::endl << "abusamir@138.52.6.250:myvoiceismypassport" << std::endl;
    }
    return 0;
}

int main()
{
    //loop forever
    while (true) {

        std::string levels[6] = { "morse", "base64", "substitution", "vigenere", "xor", "SUPER_SECRET" };

        //get input
        std::cout << "Insert Mode: ";
        std::string input;
        std::getline(std::cin, input);

        //jump to relevant level
        if (input == levels[0]) { level_one(); }
        else if (input == levels[1]) { level_two(); }
        else if (input == levels[2]) { level_three(); }
        else if (input == levels[3]) { level_four(); }
        else if (input == levels[4]) { level_five(); }
        else if (input == levels[5]) { level_hidden(); }
        else if ((input == "help") or (input == "HELP") or (input == "Help")) { 
            std::cout << "You don't need help, you're 5 times cooler than the ACC" << std::endl; 
            std::cout << "Options: morse, base64, substitution, vigenere, xor" << std::endl;
        }
        else if ((input == "quit") or (input == "QUIT") or (input == "Quit")) { exit(0); }
        else { std::cout << "Command not recognised. To quit, enter 'quit'" << std::endl; }
    }
    return 0;
}

